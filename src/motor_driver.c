#include <Arduino.h>

#include "configurations.h"
#include "motor_driver.h"

#define MOTOR_DRIVER_IN2A_PIN PB0
#define MOTOR_DRIVER_IN1A_PIN PB1
#define MOTOR_DRIVER_PWMA_PIN PC2

#define MOTOR_DRIVER_IN2B_PIN PC4
#define MOTOR_DRIVER_IN1B_PIN PC3
#define MOTOR_DRIVER_PWMB_PIN PC1

/**
 * @brief Set the directional pins state to low or high to
 * control if the motor shaft rotates
 * clockwise or counterclockwise
 * @param side
 * @param duty_cycle -255 to 255
 */
static void set_directional_pins_state(const Side side, const int16_t duty_cycle)
{
    switch (side)
    {
    case LEFT:
        if (duty_cycle > 0)
        {
            digitalWrite(MOTOR_DRIVER_IN1A_PIN, LOW);
            digitalWrite(MOTOR_DRIVER_IN2A_PIN, HIGH);
            return;
        }
        digitalWrite(MOTOR_DRIVER_IN1A_PIN, HIGH);
        digitalWrite(MOTOR_DRIVER_IN2A_PIN, LOW);
        return;
    case RIGHT:
        if (duty_cycle > 0)
        {
            digitalWrite(MOTOR_DRIVER_IN1B_PIN, LOW);
            digitalWrite(MOTOR_DRIVER_IN2B_PIN, HIGH);
            return;
        }
        digitalWrite(MOTOR_DRIVER_IN1B_PIN, HIGH);
        digitalWrite(MOTOR_DRIVER_IN2B_PIN, LOW);
        return;
    default:
        break;
    }
}

/**
 * @brief Applies saturation threshold to received duty cycle
 * @param duty_cycle -255 to 255
 */
static uint8_t get_filtered_signal(const int16_t duty_cycle)
{
    if (duty_cycle > DRIVER_MAX_DUTY_CYCLE || duty_cycle < -DRIVER_MAX_DUTY_CYCLE)
    {
        return DRIVER_MAX_DUTY_CYCLE;
    }
    return ((uint8_t)abs(duty_cycle));
}

/**
 * @brief Set the pwm duty cycle for the selected channel of the
 * driver
 * @param side
 * @param duty_cycle -255 to 255
 */
static void set_pwm_duty_cycle(Side side, uint8_t duty_cycle)
{
    switch (side)
    {
    case LEFT:
        analogWrite(MOTOR_DRIVER_PWMA_PIN, duty_cycle);
        break;
    case RIGHT:
        analogWrite(MOTOR_DRIVER_PWMB_PIN, duty_cycle);
        break;
    default:
        break;
    }
}

void set_motor_left_pwr(int16_t duty_cycle)
{
    uint8_t _duty_cycle = get_filtered_signal(duty_cycle);
    set_directional_pins_state(LEFT, duty_cycle);
    set_pwm_duty_cycle(LEFT, _duty_cycle);
}

void set_motor_right_pwr(int16_t duty_cycle)
{
    uint8_t _duty_cycle = get_filtered_signal(duty_cycle);
    set_directional_pins_state(RIGHT, duty_cycle);
    set_pwm_duty_cycle(RIGHT, _duty_cycle);
}

void init_motor_driver_control(void)
{
    pinMode(MOTOR_DRIVER_IN1A_PIN, OUTPUT);
    pinMode(MOTOR_DRIVER_IN2A_PIN, OUTPUT);
    pinMode(MOTOR_DRIVER_PWMA_PIN, OUTPUT);

    pinMode(MOTOR_DRIVER_IN1B_PIN, OUTPUT);
    pinMode(MOTOR_DRIVER_IN2B_PIN, OUTPUT);
    pinMode(MOTOR_DRIVER_PWMB_PIN, OUTPUT);
}