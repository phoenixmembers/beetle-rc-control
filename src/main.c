#include <Arduino.h>
#include "controller.h"
#include "motor_driver.h"
#include "receiver.h"



void setup()
{
  init_receiver();
  init_motor_driver_control();
}

void loop()
{
  run_robot();
}