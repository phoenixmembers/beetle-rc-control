#include <Arduino.h>

#include "configurations.h"
#include "controller.h"
#include "motor_driver.h"
#include "receiver.h"

static int16_t power_left = 0;
static int16_t power_right = 0;

static void transform_signal_to_pwm()
{
    uint32_t signal_1 = get_receiver_data()->chnl1;
    uint32_t signal_2 = get_receiver_data()->chnl2;
    
    if(signal_1 == 0){
        signal_1 = CHNL1_MID_PULSE;
    }
    else{
        if(signal_1 >= CHNL1_MAX_PULSE){
            signal_1 = CHNL1_MAX_PULSE;
        }
        else if(signal_1 <= CHNL1_MIN_PULSE){
            signal_1 = CHNL1_MIN_PULSE;
        }
    }

    if(signal_2 == 0){
        signal_2 = CHNL2_MID_PULSE;
    }
    else{
        if(signal_2 >= CHNL2_MAX_PULSE){
            signal_2 = CHNL2_MAX_PULSE;
        }
        else if(signal_2 <= CHNL2_MIN_PULSE){
            signal_2 = CHNL2_MIN_PULSE;
        }
    }
 

#ifdef SIGNAL_MAPPING_LINEAR 
    signal_1 =
        map(signal_1, CHNL1_MIN_PULSE, CHNL1_MAX_PULSE, -U8_MAX, U8_MAX);
    signal_2 =
        map(signal_2, CHNL2_MIN_PULSE, CHNL2_MAX_PULSE, -U8_MAX, U8_MAX);
#else 


#endif

    if (signal_1 < (DRIVER_MAX_DUTY_CYCLE / 10))
        signal_1 = 0;

    if (signal_2 < (DRIVER_MAX_DUTY_CYCLE / 10))
        signal_2 = 0;

    power_left = ((int16_t)(signal_1 + signal_2) / 2);
    power_right = ((int16_t)(signal_1 - signal_2) / 2);
}

void run_robot()
{
    receiver_read_channels();
    transform_signal_to_pwm();
    set_motor_left_pwr(power_left);
    set_motor_right_pwr(power_right);
}

void init_controller()
{
    init_motor_driver_control();
    init_receiver();
}
