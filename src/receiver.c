#include <Arduino.h>

#include "receiver.h"
#include "configurations.h"

#include <stdbool.h>

#define RECEIVER_CHNL_1 PD4
#define RECEIVER_CHNL_2 PD3

/*
 * Falcão asked for an exponential speed controller
 */
static receiver_data_t receiver;
static uint32_t failsafe_counter;

void receiver_read_channels()
{
    receiver.chnl1 = pulseInLong(RECEIVER_CHNL_1, 
                                HIGH, 
                                RECEIVER_SIGNAL_TIMEOUT_MICROS);
                                
    receiver.chnl2 = pulseInLong(RECEIVER_CHNL_2, 
                                HIGH, 
                                RECEIVER_SIGNAL_TIMEOUT_MICROS);
}

receiver_data_t *get_receiver_data()
{
    return &receiver;
}

void init_receiver()
{
    pinMode(RECEIVER_CHNL_1, INPUT);
    pinMode(RECEIVER_CHNL_2, INPUT);

    // Bind for receiver is not controlled via firmware
    // The firmware assumes receiver is binded
    receiver.state = BINDED;
}