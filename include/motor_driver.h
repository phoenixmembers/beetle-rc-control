#ifndef __MOTOR_DRIVER_H__
#define __MOTOR_DRIVER_H__

#include <stdint.h>

#include "commons.h"

/**
 * @brief The pwm resolution is 8 bits
 */
#define DRIVER_MAX_POWER U8_MAX

/**
 * @brief Set left motor power
 * @param duty_cycle -255 to 255
 */
void set_motor_left_pwr(int16_t duty_cycle);

/**
 * @brief Set right motor power
 * @param duty_cycle -255 to 255
 */
void set_motor_right_pwr(int16_t duty_cycle);

/**
 * @brief Set the pins connected to L6206 driver to output
 */
void init_motor_driver_control(void);

#endif /* __MOTOR_DRIVER_H__ */