#ifndef __RECEIVER_H__
#define __RECEIVER_H__

#include <stdint.h>



typedef enum
{
    UNBINDED,
    BINDED,
} BIND_STATE;

typedef struct
{
    BIND_STATE state;
    uint32_t chnl1;
    uint32_t chnl2;
} receiver_data_t;

void receiver_read_channels();

void init_receiver();

receiver_data_t *get_receiver_data();

#endif /* __RECEIVER_H__ */
