#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <stdint.h>

/********************* CONFIGURATION PARAMETERS ***********************/

/**
 * @brief This macro is used to select the signal mapping algorithm
 * used in compile time
 * @details Current supported modes are
 * - 0 Linear function
 * - 1 Exponential function
 */
#define SIGNAL_MAPPING_SELECT 0

#if SIGNAL_MAPPING_SELECT == 0
#define SIGNAL_MAPPING_LINEAR
#else
#define SIGNAL_MAPPING_EXPO
#endif

/**
 * @brief This macro sets the saturation value for the pwm signal that
 * goes to the selected driver CI
 */
#define DRIVER_MAX_DUTY_CYCLE U8_MAX 
#define DRIVER_MIN_DUTY_CYCLE 0

/**
 * @brief Timeout for receiving pulse signal in microseconds (ms)
 */
#define RECEIVER_SIGNAL_TIMEOUT_MICROS 1000000UL

#define CHNL1_MIN_PULSE 1012
#define CHNL1_MAX_PULSE 1952
#define CHNL1_MID_PULSE ((CHNL1_MAX_PULSE + CHNL1_MIN_PULSE) / 2)

#define CHNL2_MIN_PULSE 1056
#define CHNL2_MAX_PULSE 1904
#define CHNL2_MID_PULSE (((CHNL2_MAX_PULSE + CHNL2_MIN_PULSE) / 2))
/********************* CONFIGURATION VALIDATION ***********************/

#if ((DRIVER_MAX_DUTY_CYCLE > U8_MAX || DRIVER_MAX_DUTY_CYCLE < 0) || (DRIVER_MIN_DUTY_CYCLE > U8_MAX || DRIVER_MIN_DUTY_CYCLE < 0))
#error "[CFG] DRIVER_MAX_POWER_MACRO incorrect value, set  value to less than U8_MAX"
#endif

#if CHNL1_MAX_PULSE > U32_MAX || CHNL2_MAX_PULSE > U32_MAX
#error "[CFG] CHNL_MAX_PULSE OVERFLOW, set value to less than U32_MAX"
#endif

#endif /* __CONFIGURATION_H__ */