# Documentação Código combate RC
## Spread the fire!
![Phoenix logo](extra/logo_laranja_fundo_preto.jpg)

# Introdução
Código de controle por sinais de rádio usando o microcontrolador [STM8S207K8T3C][microcontroller-datasheet] na placa [Little Endian 3.0][altium-board] com Framework Arduino.
## Resumo 
O código do combate controla os motores utilizando o periodo dos pulsos recebidos pelo receptor de ondas de rádio, utilizando 2 canais para fazer mixagem dos sinais recebidos.

## Arquivos 
* motor_driver.c: Responsável pela lógica de controle dos motores e os drivers.
* receiver.c: Responsável pela recepção do sinal do receptor de rádio
* controller.c: Neste arquivo é implementado a interpretação do sinal obtido do receptor, assim como a transformação desse sinal num pwm valido para o driver
## Configuração
O arquivo configurations.h contém os parametros que devem ser revisados e ajustados antes de programar o micro.\
Existem 3 parametros configuráveis:
* Duty cycle máximo e minimo que será considerado para gerar o pwm, variando no intervalo [0 255]
* Periodo maximo e minimo do pulso do receptor
* Tipo de mapeamento do sinal recebido para pwm 
  * Modo linear 
  * Modo exponencial aproximado (TODO)

[microcontroller-datasheet]: https://www.mouser.com/datasheet/2/389/stm8s207mb-1851297.pdf
[altium-board]: https://phoenixequipe-phoenix-de-robtica-da-unicamp-5.365.altium.com/designs/795ED25C-DCA7-4334-99CA-49E8951B5868?variant=[No+Variations]&activeView=SCH&activeDocumentId=HTZGTOGW